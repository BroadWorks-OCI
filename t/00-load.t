#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'BroadWorks::OCI' ) || print "Bail out!\n";
}

diag( "Testing BroadWorks::OCI $BroadWorks::OCI::VERSION, Perl $], $^X" );
